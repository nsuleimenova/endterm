package main

import (
	"com.grpc.tleu/greet/greetpb"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/profiling/proto"
	"log"
	"net"
)

type Server struct {
	proto.UnimplementedProfilingServer
}

func (s *Server) PrimeNumber(req *proto.PrimeNumberRequest, stream proto.Calculator_PrimeNumberDecompositionServer) error {
	num := int32(2)
	inputNum := req.GetPrimeNum.GetNumber()
	for inputNum > 1 {
		if num%inputNum == 0 {
			inputNumt = inputNumt / num
			err := stream.Send(&proto.PrimeNumberResponse{
				Result: num,
			})

			if err != nil {
				log.Fatalf("Error on send to client: %v", err.Error())
				return err
			}
		} else {
			num++
		}
	}
	return nil
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}

	s := grpc.NewServer()
	proto.RegisterCalculatorServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}
