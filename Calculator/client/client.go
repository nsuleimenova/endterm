package main

import (
	"com.grpc.tleu/Calculator/proto"
	"context"
	"fmt"
	"io"
)

import (
	"google.golang.org/grpc"
	"log"
)

func main() {
	fmt.Println("Hello I'm a client")

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("can not connect: %v", err)
	}
	defer conn.Close()

	c := proto.NewCalculatorServiceClient(conn)
	doPrimeNum(c)
}

func doPrimeNum(c proto.CalculatorServiceClient) {
	stream, err := c.PrimeNumberDecomposition(context.Background(), &proto.PrimeNumRequest{
		PrimeNumber: 120,
	})
	if err != nil {
		log.Fatalf("Error while open stream: %v", err.Error())
	}

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("error while getting message from stream: %v", err.Error())
		}
		log.Printf("THE PRIME NUMBER IS: %v", res.GetResult())
	}

}
